<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> P04 - Página principal </title>
    </head>
    <body>
        <?php
            error_reporting(E_ERROR);
            include("p04_funciones.php");
            /*Ejercicio 1 */
            echo "<h3>Ejercicio 1</h3>";
            $num1 = $_GET['numero'];
            multiplo5_7($num1);
            /*Ejercicio 2 */
            echo "<h3>Ejercicio 2</h3>";
            random_matriz();
            /*Ejercicio 3 */
            echo "<h3>Ejercicio 3</h3>";
            $multiplo = $_GET['multiplo'];
            random_multiplo($multiplo);
            /*Ejercicio 4 */
            echo "<h3>Ejercicio 4</h3>";
            array_letras();
        ?>

    </body>
</html>