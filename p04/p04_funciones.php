
<?php
    /*Ejercicio 1*/
    function multiplo5_7($num1)
    {   
        if(isset($num1)){
            if($num1 % 5 == 0 && $num1 % 7 == 0)
                echo "<p>El número $num1 es múltiplo de 5 y 7.</p>";
            else
            echo "<p>El número $num1 no es múltiplo de 5 y 7.</p>";
        }else{
            echo "<p>La variable no ha sido definida.</p>";
        }
    }

    /*Ejercicio 2*/
    function random_matriz(){
        $i= 0; $j=0;
        $matriz;
        do{
            do{
                $matriz[$i][$j] = rand(1,1000);
                $j++;
            }while($j!=3);
            $j=0;
            if(($matriz[$i][0] % 2 != 0) && ($matriz[$i][1] % 2 == 0) && ($matriz[$i][2] % 2 != 0)){
                break;
            }else{
                $i++;
            }  
        }while(true);
        
        $numeros = 0;
        foreach($matriz as $valor){
            $numeros = $numeros + count($valor);
            foreach($valor as $num){
                echo "<span>".$num."&nbsp;&nbsp;&nbsp;&nbsp;</span>";
            }
            echo "<br>";
        }
        echo "<p>impar&nbsp;&nbsp;par&nbsp;&nbsp;impar</p>";
        $iteraciones = $i+1;
        echo '<p>'.$numeros.' números obtenidos en '.$iteraciones.' iteraciones.</p>';
    }

    /*Ejercicio 3*/
    function random_multiplo($multiplo){
        /*Script con while */
        if(isset($multiplo)){
            while(true){
                $num1= rand(1,1000)/10;
                if(is_int($num1) && $num1%$multiplo == 0){
                    echo "<p> <b>Hecho con while:</b> El número ".$num1." es entero y es múltiplo de ".$multiplo.".</p>";
                    break;
                } 
            }
            /*Script con do-while */
            do{
                $num1= rand(1,1000)/10;
                if(is_int($num1) && $num1%$multiplo == 0){
                    echo "<p> <b>Hecho con do-while:</b> El número ".$num1." es entero y es múltiplo de ".$multiplo.".</p>";
                    break;
                } 
            }while(true);            
        }else{
            echo "<p>La variable no ha sido definida.</p>";
        }
    }
    
    /*Ejercicio 4*/
    function array_letras(){
        $arreglo;
        for($i=97;$i<=122;$i++){
            $arreglo[$i] = chr($i);
        }
        echo "<table style=\"border: blue 1.5px solid;\">";
        foreach($arreglo as $key => $value){
            echo "<tr style=\"border: blue 1px solid;\"><td style=\"border: blue 1px solid;\">".$key."</td><td style=\"border: blue 1px solid;\">".$value."</td></tr>";
        }
        echo "</style></table>";
    }

?>
