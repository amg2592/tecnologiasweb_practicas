<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> Ejercicio 05 </title>
    </head>
    <body>
        <?php
            error_reporting(E_ERROR);
            $edad=intval($_POST['edad']);
            $sexo=$_POST['sexo'];
            if(isset($edad) && isset($sexo)){
                echo "<h2>Ejercicio 5</h2>";
                if(($edad > 17) && ($edad < 36) && $sexo == 'Femenino'){
                    echo "<p>Bienvenida, usted está en el rango de edad permitido.<p>";
                }
                else{
                    echo "<p>Una disculpa, usted no cumple los requerimientos solicitados.<p>";
                }
            }
        ?>
    </body>
</html>
