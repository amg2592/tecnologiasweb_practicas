<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> Ejercicio 06 </title>
    </head>
    <body>
        <?php
            error_reporting(E_ERROR);
            $array_autos = array(
                'UAX1351' => array(
                    'Auto' => array(
                        'marca'=>'Chevrolet',
                        'modelo'=>'2016',
                        'tipo' => 'sedan'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Alhelí Gaytán',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'Bosques de Amalucan, Galaxia'
                    )
                ),
                'JHY0012' => array(
                    'Auto' => array(
                        'marca'=>'Honda',
                        'modelo'=>'2010',
                        'tipo' => 'camioneta'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Jorge Moreno',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'Satélite Magisterial'
                    )
                ),
                'XYV9256' => array(
                    'Auto' => array(
                        'marca'=>'Volkswagen',
                        'modelo'=>'2020',
                        'tipo' => 'camioneta'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Beatriz Martínez',
                        'ciudad' => 'Guadalupe Victoria, Puebla',
                        'direccion' => 'La Concepción'
                    )
                ),
                'CYU0205' => array(
                    'Auto' => array(
                        'marca'=>'Hyundai',
                        'modelo'=>'2006',
                        'tipo' => 'sedan'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Humberto Fernández',
                        'ciudad' => 'Tlaxcala, Tlax',
                        'direccion' => '4 norte no.30, Centro'
                    )
                ),
                'AMJ0311' => array(
                    'Auto' => array(
                        'marca'=>'MG',
                        'modelo'=>'2018',
                        'tipo' => 'hachback'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Fernando Martínez',
                        'ciudad' => 'Puebla, Puebla',
                        'direccion' => 'Santa Bárbara Sur'
                    )
                ),
                'XXO7895' => array(
                    'Auto' => array(
                        'marca'=>'Susuki',
                        'modelo'=>'2021',
                        'tipo' => 'camioneta'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Aarón González',
                        'ciudad' => 'San Andrés Cholula, Pue',
                        'direccion' => 'Paso de Cortés'
                    )
                ),
                'FNA1318' => array(
                    'Auto' => array(
                        'marca'=>'Toyota',
                        'modelo'=>'2015',
                        'tipo' => 'sedan'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Daniel Cruz',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'La libertad'
                    )
                ),
                'GHT8956' => array(
                    'Auto' => array(
                        'marca'=>'Honda',
                        'modelo'=>'2017',
                        'tipo' => 'camioneta'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Karina Ortiz',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'México 68'
                    )
                ),
                'RTI4512' => array(
                    'Auto' => array(
                        'marca'=>'BMW',
                        'modelo'=>'2022',
                        'tipo' => 'sedan'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Santiago López',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'Universidades'
                    )
                ),
                'UYH7869' => array(
                    'Auto' => array(
                        'marca'=>'Audi',
                        'modelo'=>'2022',
                        'tipo' => 'hachback'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Sara Gómez',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'La paz no.158'
                    )
                ),
                'MKO1475' => array(
                    'Auto' => array(
                        'marca'=>'Lincoln',
                        'modelo'=>'2021',
                        'tipo' => 'hachback'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Juan Islas',
                        'ciudad' => 'Tlachichuca, Pue',
                        'direccion' => '2 norte no.7, Centro'
                    )
                ),
                'UUY3698' => array(
                    'Auto' => array(
                        'marca'=>'Chevrolet',
                        'modelo'=>'2014',
                        'tipo' => 'sedan'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Nelly Méndez',
                        'ciudad' => 'San Pedro Cholula, Pue',
                        'direccion' => '12 sur no.59'
                    )
                ),
                'OIU0203' => array(
                    'Auto' => array(
                        'marca'=>'Nissan',
                        'modelo'=>'2008',
                        'tipo' => 'camioneta'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Raúl Pérez',
                        'ciudad' => 'Libres, Puebla',
                        'direccion' => 'Constitución 130'
                    )
                ),
                'TUY1182' => array(
                    'Auto' => array(
                        'marca'=>'Seat',
                        'modelo'=>'2021',
                        'tipo' => 'sedan'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'José Luis Cosme',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'Los Héroes'
                    )
                ),
                'TVR3572' => array(
                    'Auto' => array(
                        'marca'=>'NMercedes Benz',
                        'modelo'=>'2019',
                        'tipo' => 'camioneta'
                    ),
                    'Propietario'=>array(
                        'nombre'=> 'Adriana Santos',
                        'ciudad' => 'Puebla, Pue',
                        'direccion' => 'La Calera 133'
                    )
                ),
            );
            //print_r($array_autos);
            $matricula=$_POST['matricula'];
            $contador = 1;
            if(isset($matricula) && $matricula != null){
                foreach($array_autos as $key => $value){
                    if($key == $matricula){
                        echo "<h3>Información Auto</h3>";
                        echo "Matrícula: ".$key."<br>";
                        echo "Marca: ".$value['Auto']['marca']."<br>";
                        echo "Modelo: ".$value['Auto']['modelo']."<br>";
                        echo "Tipo: ".$value['Auto']['tipo']."<br>";
                        echo "Propietario: ".$value['Propietario']['nombre']."<br>";
                        echo "Propietario: ".$value['Propietario']['ciudad']."<br>";
                        echo "Propietario: ".$value['Propietario']['direccion']."<br>";
                        break;
                    }
                }          
            }else{
                foreach($array_autos as $key => $value){
                        echo "<h3>Auto ".$contador."</h3>";
                        echo "Matrícula: ".$key."<br>";
                        echo "Marca: ".$value['Auto']['marca']."<br>";
                        echo "Modelo: ".$value['Auto']['modelo']."<br>";
                        echo "Tipo: ".$value['Auto']['tipo']."<br>";
                        echo "Propietario: ".$value['Propietario']['nombre']."<br>";
                        echo "Ciudad: ".$value['Propietario']['ciudad']."<br>";
                        echo "Dirección: ".$value['Propietario']['direccion']."<br><hr>";
                        $contador++;
                }                
            }
        ?>
    </body>
</html>